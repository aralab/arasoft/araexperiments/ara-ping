import socket
import sys
from datetime import datetime

PORT = 7788


def serve():
    server_ip = sys.argv[1]

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((server_ip, PORT))

    while True:
        # Receive packets from the UEs
        data, addr = sock.recvfrom(1024)
        recv_data = data.decode()
        ts = datetime.now().timestamp()

        # Creating the response packet
        data_frame = f'{recv_data}:{ts}'.encode()

        # Sending the response packets to the UE
        sock.sendto(data_frame, addr)


if __name__ == '__main__':
    serve()
