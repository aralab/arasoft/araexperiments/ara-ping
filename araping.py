import socket
import sys
import threading
import time
from datetime import datetime

PORT = 7788
delay_list = []


# Function to receive response packets from the core network
def recv_frame(sock, no_of_packets):
    for i in range(no_of_packets):
        try:
            # Receive data from core network
            data, addr = sock.recvfrom(1024)
            current_time = datetime.now().timestamp()
            recv_data = data.decode()

            # Computing the uplink and downlink delay
            comps = recv_data.split(':')
            sent_time = float(comps[1])
            core_recv_time = float(comps[2])
            uplink_delay = core_recv_time - sent_time
            downlink_delay = current_time - core_recv_time
            delay_list.append((uplink_delay, downlink_delay))
        except socket.timeout:
            break


# Function to send data packets to the core network
def send_frame(sock, core_ip, no_of_packets, interval):
    for i in range(no_of_packets):
        continue
        # Sending data frame to the core network with sequence number
        data_frame = f'{i}:{datetime.now().timestamp()}'.encode()
        sock.sendto(data_frame, (core_ip, PORT))
        time.sleep(interval)


if __name__ == '__main__':
    ue_ip = sys.argv[1]
    core_ip = sys.argv[2]

    no_of_packets = int(sys.argv[3])
    interval = float(sys.argv[4])

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((ue_ip, PORT))
    sock.settimeout(10)

    # Start a thread to receive the response packets from core network
    recv_thread = threading.Thread(target=recv_frame,
                                   args=(sock, no_of_packets,))
    recv_thread.start()

    # Send packets to the core network
    send_frame(sock, core_ip, no_of_packets, interval)

    recv_thread.join()
    sock.close()

    # Computing the average uplink and downlink delay and packet loss
    ul_delay_sum = sum(i for i, j in delay_list)
    dl_delay_sum = sum(j for i, j in delay_list)

    try:
        ul_delay_avg = ul_delay_sum / len(delay_list)
        dl_delay_avg = dl_delay_sum / len(delay_list)
    except ZeroDivisionError:
        ul_delay_avg, dl_delay_avg = 'Inf', 'Inf'

    packet_loss = (no_of_packets - len(delay_list)) / no_of_packets

    print(f'Average uplink delay (sec): {ul_delay_avg}\
          \nAverage downlink delay (sec): {dl_delay_avg}\
          \nPacket loss (%): {packet_loss * 100}')
